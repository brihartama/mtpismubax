<?php 


class Detail extends CI_Controller{
    
    public function __construct() 
        { 
            parent::__construct();
            $this->load->model('m_buku');
            $this->load->helper('url'); 
        }
        
    public function index()
	{
        $this->load->view("customer/detail/v_detail");
    }

    public function lihat_barang($idbarang){
            $data['detailbuku'] = $this->m_buku->detail_buku($idbarang);
            $this->load->view("customer/detail/v_detail", $data);
    }



    
}

?>