<?php
class Laporan extends CI_Controller{
	function __construct(){
        parent::__construct();
        $this->load->model('m_transaksi');
        $this->load->model('m_laporan');
		$this->load->model('m_sekolah');
		$this->load->library('upload');
	}


	function index(){
        
        $data['tampilsemua'] = $this->m_laporan->tampil_semua();
        $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual();
        $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan();
        $data['total']=$this->m_transaksi->banyak_transaksi();
        $data['kabupaten']=$this->m_laporan->getkabupaten();
        $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten();
        $data['pilihkabupaten'] = $this->m_laporan->pilih_kabupaten();
        $data['laporanbuku'] = $this->m_laporan->laporan_buku();
		$this->load->view('admin/laporan/v_tampil_laporan', $data);
    }

    function cetak_laporan(){
        $data['tampilsemua'] = $this->m_laporan->tampil_semua();
        $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual();
        $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan();
        $data['total']=$this->m_transaksi->banyak_transaksi();
        $data['kabupaten']=$this->m_laporan->getkabupaten();
        $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten();
        $data['laporanbuku'] = $this->m_laporan->laporan_buku();
        $this->load->view('admin/laporan/v_cetak', $data);
    }

    function ambil_id_kabupaten($idkab){
        $this->m_laporan->ambil_total($idkab);
    }


    //semua di bawah ini function buat cetak laporan perkabupaten

    function cetak_laporan_kabupaten(){
        $idkabupten = $this->input->post("xidkabupaten");
        $bulandari = $this->input->post("xbulan1");
        $bulansampai = $this->input->post("xbulan2");
        $tahundari = $this->input->post("xtahun1");
        // $tahunsampai = $this->input->post("xtahun2");
        $data['tampilsemua_kab'] = $this->m_laporan->tampil_semua_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['nama_sekolah_kab'] = $this->m_laporan->nama_sekolah_kabupaten($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['totalbukuterjual_kab'] = $this->m_laporan->total_buku_terjual_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['totalpendapatanpenjualan_kab'] = $this->m_laporan->total_pendapatan_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['databuku'] = $this->m_laporan->banyak_buku();
        $this->load->view("admin/laporan/v_cetak_laporan_kab.php", $data);
    }

    function cetak_laporan_buku(){
        $tahun = $this->input->post("xtahun1");
        $tahunlama = $tahun-1;
        $data['bukutahun'] = $this->m_laporan->stok_2019($tahun);
        $data['databuku'] = $this->m_laporan->laporan_bukuku($tahun);
        $data['rekapan'] = $this->m_laporan->rekapan_buku($tahun);
        $data['laporansisa'] = $this->m_laporan->laporan_sisa($tahun);
        $data['tahun'] = $tahun;
        $data['tahunlama'] = $tahunlama;
        $data['filterlevel'] = $this->m_laporan->filter_lelve();
        $this->load->view("admin/laporan/v_cetak_buku.php", $data);
        
    }

    function filter() {
        $this->load->view("admin/laporan/v_filter.php");
    }

    function cetak_laporan_semua(){
        $tahundari = $this->input->post("xtahun1");
        $tahunsampai = $this->input->post("xtahun2");
        $data['tampilsemua'] = $this->m_laporan->tampil_semua_x($tahundari, $tahunsampai);
        $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual_x($tahundari, $tahunsampai);
        $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan_x($tahundari, $tahunsampai);
        $data['total']=$this->m_transaksi->banyak_transaksi_x($tahundari, $tahunsampai);
        $data['kabupaten']=$this->m_laporan->getkabupaten_x($tahundari, $tahunsampai);
        $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten_x($tahundari, $tahunsampai);
        $data['laporanbuku'] = $this->m_laporan->laporan_buku_x($tahundari, $tahunsampai);
        $this->load->view('admin/laporan/v_cetak_laporan', $data);
        
    }


    
}