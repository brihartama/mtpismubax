<?php
class Buku1 extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_buku');
        $this->load->library('form_validation');
        $this->load->library('upload');
	}


	function index(){
        $data["buku"] = $this->m_buku->getAll();
        $this->load->view("admin/buku/v_tampil_buku", $data);
	}
    
    public function add(){
        $this->load->view("admin/buku/v_tambah_buku");
    }

    public function edit($id){
        $where = array('id_buku'=>$id);
        $data['buku']=$this->m_buku->edit_buku($where, 'tbl_buku');
        $this->load->view("admin/buku/v_edit_buku", $data);
    }

    public function update_buku(){

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$this->input->post('gambar');
                $path='./assets/images/'.$gambar;
                unlink($path);

                $photo=$gbr['file_name'];
                $id=$this->input->post('xid');
                $nama=$this->input->post('xnama');
                $kelas = $this->input->post('xkelas');
                $harga = $this->input->post('xharga');
                $stok = $this->input->post('xstok');
                $this->m_buku->update_buku($id, $nama, $kelas, $harga, $stok, $photo);
                $this->m_buku->update_laporan_stok($id,$stok);
                redirect('admin/buku1/');
            }else{
                redirect('admin/buku1/');
            }

        }else{

            $id=$this->input->post('xid');
            $nama=$this->input->post('xnama');
            $kelas = $this->input->post('xkelas');
            $harga = $this->input->post('xharga');
            $stok = $this->input->post('xstok');
            $this->m_buku->update_buku_tanpa_img($id, $nama, $kelas, $harga, $stok);
            $this->m_buku->update_laporan_stok($id,$stok);
            redirect('admin/buku1/');
        }



        $id=$this->input->post('xid');
        $nama=$this->input->post('xnama');
        $kelas = $this->input->post('xkelas');
        $harga = $this->input->post('xharga');
        $stok = $this->input->post('xstok');
        $this->m_buku->update_buku($id, $nama, $kelas, $harga, $stok);
        redirect('admin/buku1/');
        
    }

    public function hapus($id){
        $where = array('id_buku'=>$id);
        $this->m_buku->hapus($where, 'buku');
        redirect('admin/buku1/');
        
    }

    public function simpan_buku(){

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $kelas = $this->input->post("xkelas");
                $harga = $this->input->post("xharga");
                $stok = $this->input->post("xstok");
                $this->m_buku->simpan_buku($nama, $kelas, $harga, $stok,$photo);
                $idbukunya = $this->m_buku->ambil_id();
                $this->m_buku->simpan_stok($idbukunya, $stok);
                redirect('admin/buku1/');
            }else{
                redirect('admin/buku1/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $kelas = $this->input->post("xkelas");
            $harga = $this->input->post("xharga");
            $stok = $this->input->post("xstok");
            $this->m_buku->simpan_buku_tanpa_image($nama, $kelas, $harga, $stok);
            $idbukunya = $this->m_buku->ambil_id();
            $this->m_buku->simpan_stok($idbukunya, $stok);
            redirect('admin/buku1/');
        }
    }

    public function delete_buku(){
        $id = $this->input->post('xid');
		$gambar=$this->input->post('foto_barang');
		$path='./assets/images/'.$gambar;
        unlink($path);
        
        $this->m_buku->delete_buku($id);
        redirect('admin/buku1/');
    }

}