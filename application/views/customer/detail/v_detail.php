<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">


</head>
<body>

<!-- Menu -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<div class="super_container">

	<!-- Header -->

	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->
	

		<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center" style="margin-top: 1.5em; font-family: 'Overpass', sans-serif;">Detail
						</div>
						<hr style="border: 1px solid #757575; opacity: 0.2;">
					</div>
				</div>
				

					
	<div class="detail-center"> 
		<?php foreach($detailbuku as $data):?>
			  <div class="wrapperd">
    <div class="productd-img">
      <img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>">
    </div>

    <div class="productd-info">
      <div class="productd-text">
        <h1><?php echo $data->nama_buku?></h1>

        <h2>Kategori <?php if($data->kelas_buku <8){
                          echo ": SD/MI";
                        }elseif ($data->kelas_buku >7 AND $data->kelas_buku <10){
                          echo ": SMP/MTS";
                        }else{
                          echo ": SMA/MA/SMK";
                        }?></h2>
        
        	<p>Kelas	:	<?php echo $data->kelas_buku ?>
        	<br>Penerbit	:	ISMUBA 2019
          	<br>Tahun	:	Terbit 2019 </p>

      </div>
      <div class="productd-price-btn">
        <p><i class="fa fa-tag"></i>Rp. <?php echo $data->harga_buku?></p>

        <a href="<?php echo site_url('customer/cart/tambah_barang/'.$data->id_buku)?>">
        <button type="button">ADD TO CART</button>
      </a>
      </div>
    </div>
  </div>
			</div>
		<?php endforeach?>
		</div>
	</div>

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>