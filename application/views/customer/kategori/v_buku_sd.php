<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.carousel.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">


</head>
<body>

<!-- Menu -->



<div class="super_container">

	<!-- Header -->
	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>

	<div class="super_container_inner">
		<div class="super_overlay"></div>
		<!-- Products -->

		<div class="products" style="background-color: #f5f5f5">
			<div class="containerx" style="margin-top: 4vw;">
				<div class="row">
					<div class="col col-lg-6 offset-lg-3">
						<div class="section_title text-center"> Produk
							<hr style="border-top: .9px solid grey"></div>
					</div>
				</div>
				<div class="row products_row">

					
					<!-- Product -->
					<div class="product-center">

				<?php foreach($buku as $data):?>
					<a href="<?php echo site_url('customer/detail/lihat_barang/'.$data->id_buku)?>" style = "text-decoration: none;">


								
					<div class="columnx">
						
						<div class="productx">
							<div class="productx-image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>"/></div>

							<div class="productx-detail">
								<div class="">
									<div>
										<div>
											<div class="productx-name"> <?php echo $data->nama_buku?> </div>

											<div class="productx-cns" style="color: #111"> Kelas : <?php echo $data->kelas_buku?> | 
										Stok : <?php echo $data->stok?></div>
										</div>

										<center>

										<div class="productx-price"><span>Rp. </span><span></span><?php echo $data->harga_buku?></div></center>

									</div>
								</div>
								
							
								
							</div>
							</a>
						</div>
					</div>

				<?php endforeach?>

				</div>

				</div>
				<div class="row load_more_row">
					<div class="col">
						<div class="button load_more ml-auto mr-auto"><a href="#">load more</a></div>
					</div>
				</div>
			</div>
		</div>

		<!-- Boxes -->

		<div class="boxes">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="boxes_container d-flex flex-row align-items-start justify-content-between flex-wrap">

							<!-- Box -->
							<div class="box">
								<div class="background_image" style="background-image:url(assets2/images/hehe.jpg); filter: blur(3px);"></div>
								<div class="box_content d-flex flex-row align-items-center justify-content-start">
									<div class="box_left">
										<div class="box_image">
											<a href="category.html">
												<div class="background_image" style="background-image:url(assets2/images/hehe.jpg);"></div>
											</a>
										</div>
									</div>
									<div class="box_right text-center">
										<div class="box_title">Dummy Widget</div>
									</div>
								</div>
							</div>

							<!-- Box -->
							<div class="box">
								<div class="background_image" style="background-image:url(assets2/images/hehe.jpg); filter: blur(3px);"></div>
								<div class="box_content d-flex flex-row align-items-center justify-content-start">
									<div class="box_left">
										<div class="box_image">
											<a href="category.html">
												<div class="background_image" style="background-image:url(assets2/images/hehe.jpg)"></div>
											</a>
										</div>
									</div>
									<div class="box_right text-center">
										<div class="box_title">Dummy Widget</div>
									</div>
								</div>
							</div>

							<!-- Box -->
							<div class="box">
								<div class="background_image" style="background-image:url(assets2/images/hehe.jpg); filter: blur(3px);;"></div>
								<div class="box_content d-flex flex-row align-items-center justify-content-start">
									<div class="box_left">
										<div class="box_image">
											<a href="category.html">
												<div class="background_image" style="background-image:url(assets2/images/hehe.jpg)"></div>
											</a>
										</div>
									</div>
									<div class="box_right text-center">
										<div class="box_title">Dummy Widget</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Features -->

		<div class="features">
			<div class="container">
				<div class="row">
					
					

				</div>
			</div>
		</div>

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>