<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("customer/login/csslogres.php") ?>
  <title>Register Akun</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3>Register</h3>
  </center>
   </div>
   
   <div class="name" style="color: #fff">
   <?php
                  $error_msg=$this->session->flashdata('error_msg');
                  if($error_msg){
                    echo $error_msg;
                  }
                   ?>

                   </div>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('customer/login/register_customer'); ?>">
     <fieldset>
                              <div class="form-group">
                                  <input class="form-control" placeholder="Name / School's Name" name="user_name" type="text" autofocus>
                              </div>

                              <div class="form-group">
                <label for="name" style="color: #fff;">Kabupaten*</label>
                <div class="form-group">
                                        <select class="form-control" id="sel1" name="xidkabupaten" required>
                                        <?php 
                                            foreach ($kabupaten as $data):
                                        ?>
                                            <option value="<?php echo $data->id_kabupaten;?>"><?php echo $data->nama_kabupaten?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                  </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="Alamat" name="xalamat" type="text" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="E-mail" name="user_email" type="email" autofocus>
                              </div>

                              <div class="form-group">
                                  <input  class="form-control" placeholder="Number Phone" name="number_phone" type="number" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="Password" name="user_password" type="password" value="">
                              </div>


                             <div class="form-group">
      <input type="submit" value="Login" class="btn float-right login_btn">
     </div>

                          </fieldset>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links">
    <center><b>Already registered ?</b> <br></b><a href="<?php echo base_url('customer/login/login_view'); ?>"><b> Login here </b></a></center><!--for centered text-->
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
