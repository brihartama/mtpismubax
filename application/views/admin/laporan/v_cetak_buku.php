<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<div id="wrapper">
		<div id="content-wrapper">

			<div class="container-fluid">
				<!-- DataTables -->
				<div class="row">
                                     
			<!-- Sticky Footer -->
	    </div>
        <div class="container-fluid">
        <strong><i><h5>Daftar Buku</h5></i></strong>
            <div class="card text-black">
                <div class="card-body">
                <p>Laporan Penjualan Buku ISMUBA Tahun <?php echo $tahun?></p>
                <!-- <table>
                    <tr>
                        <td>Banyak Buku</td><td>1200</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SD/MI</td><td>300</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SMP/MTS</td><td>300</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SMA/MA</td><td>500</td>
                    </tr>
                </table> -->
                <!-- untuk buku level SD -->
                    <p>SD</p>
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> STOK <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td>Stok <?php echo $tahun?></td>
                                                    <td>Sisa <?php echo $tahunlama?></td>
                                                    <td >Jumlah</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data): ?>
                                                <?php if($data->kelas_buku < 7): $no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                   
                                                        
                                                            <table width=100%>
                                                            
                                                                <tr align="center" width=100%>
                                                                    <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td width=33%><?php echo $kunci->stok_awal-$kunci->stok_sisa?></td>
                                                                                    <td width=33%><?php echo ($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa)?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                                </tr>
                                                                
                                                            </table></center>
                                                            
                                                    </td>
                                                                     <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td><?php echo $key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td><?php echo (($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa))-$key->stok_sisa?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                   
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level sd -->

                <!-- untuk buku level SMP -->
                    <p>SMP</p>
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> STOK <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td>Stok <?php echo $tahun?></td>
                                                    <td>Sisa <?php echo $tahunlama?></td>
                                                    <td >Jumlah</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data): ?>
                                                <?php if($data->kelas_buku < 10 && $data->kelas_buku > 6): $no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                   
                                                        
                                                            <table width=100%>
                                                            
                                                                <tr align="center" width=100%>
                                                                    <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td width=33%><?php echo $kunci->stok_awal-$kunci->stok_sisa?></td>
                                                                                    <td width=33%><?php echo ($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa)?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                                </tr>
                                                                
                                                            </table></center>
                                                            
                                                    </td>
                                                                     <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td><?php echo $key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td><?php echo (($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa))-$key->stok_sisa?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                   
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level SMP -->

                <!-- untuk batas buku level SMA -->
                    <p>SMA</p>
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> STOK <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td>Stok <?php echo $tahun?></td>
                                                    <td>Sisa <?php echo $tahunlama?></td>
                                                    <td >Jumlah</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data): ?>
                                                <?php if($data->kelas_buku > 9):$no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                   
                                                        
                                                            <table width=100%>
                                                            
                                                                <tr align="center" width=100%>
                                                                    <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td width=33%><?php echo $kunci->stok_awal-$kunci->stok_sisa?></td>
                                                                                    <td width=33%><?php echo ($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa)?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                                </tr>
                                                                
                                                            </table></center>
                                                            
                                                    </td>
                                                                     <?php foreach($bukutahun as $key):?>
                                                                        <?php if($data->id_buku == $key->id_buku):?>
                                                                            <td><?php echo $key->stok_sisa?></td>
                                                                            <?php foreach($laporansisa as $kunci):?>
                                                                                <?php if($data->id_buku == $kunci->id_buku):?>
                                                                                    <td><?php echo (($key->stok_awal-$key->stok_sisa)+($kunci->stok_awal-$kunci->stok_sisa))-$key->stok_sisa?></td>
                                                                                <?php endif?>
                                                                            <?php endforeach?>
                                                                        <?php endif?>
                                                                    <?php endforeach?>
                                                   
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level SMA -->

                </div>
            </div>
        </div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->
    <script type="text/javascript">
        window.print();
    </script>
</body>
</html>
