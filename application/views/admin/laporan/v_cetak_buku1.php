<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<div id="wrapper">
		<div id="content-wrapper">

			<div class="container-fluid">
				<!-- DataTables -->
				<div class="row">
                                     
			<!-- Sticky Footer -->
	    </div>
        <div class="container-fluid">
        <strong><i><h5>Daftar Buku</h5></i></strong>
            <div class="card text-black">
                <div class="card-body">
                <p>Laporan Penjualan Buku ISMUBA Tahun <?php echo $tahun?></p>
                <!-- <table>
                    <tr>
                        <td>Banyak Buku</td><td>1200</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SD/MI</td><td>300</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SMP/MTS</td><td>300</td>
                    </tr>
                    <tr>
                        <td>Banyak Buku SMA/MA</td><td>500</td>
                    </tr>
                </table> -->
                <!-- untuk buku level SD -->
                    <p>SD</p>
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td>stok <?php echo $tahunlama?></td>
                                                    <td>sisa <?php echo $tahunlama?></td>
                                                    <td >terjual <?php echo $tahun?>    </td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1  width=100%>
                                                    <tr  align="center">
                                                    <td>Stok</td>
                                                    <td>Sisa</td>
                                                    <td>terjual</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data): ?>
                                                <?php if($data->kelas_buku < 7): $no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                    <?php foreach($laporansisa as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td><?php echo $key->stok_awal?></td>
                                                        <td><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td>120</td>
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td><center>
                                                    <?php foreach($data2019 as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td width=33%><?php echo $key->stok_awal?></td>
                                                        <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td width=33%><?php echo $key->stok_sisa?></td>
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td>Haha</td>
                                                    <td>haha</td>
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level sd -->

                <!-- untuk buku level SMP -->
                    <p>SMP</p>
                    <div>
                                    <table width="100%" border="1">
                                    <thead>
                                    <tr  align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td width=33%>stok <?php echo $tahunlama?></td>
                                                    <td width=33%>sisa <?php echo $tahunlama?></td>
                                                    <td width=33%>terjual <?php echo $tahun?>    </td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1  width=100%>
                                                    <tr  align="center">
                                                    <td>Stok</td>
                                                    <td>Sisa</td>
                                                    <td>terjual</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data):?>
                                                <?php if($data->kelas_buku < 10 && $data->kelas_buku > 6): $no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                    <?php foreach($laporansisa as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td><?php echo $key->stok_awal?></td>
                                                        <td><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td>120</td>
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td><center>
                                                    <?php foreach($data2019 as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td width=33%><?php echo $key->stok_awal?></td>
                                                        <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td width=33%><?php echo $key->stok_sisa?></td>
                                                        
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td>Haha</td>
                                                    <td>haha</td>
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level SMP -->

                <!-- untuk batas buku level SMA -->
                    <p>SMA</p>
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr  align="center">
                                                <th>Nomer*</th>
                                                <th>Nama </th>
                                                <th>Kelas</th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1 width=100%>
                                                    <tr  align="center">
                                                    <td width=33%>stok <?php echo $tahunlama?></td>
                                                    <td width=33%>sisa <?php echo $tahunlama?></td>
                                                    <td width=33%>terjual <?php echo $tahun?>    </td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th><center> <?php echo $tahun?>
                                                <table border=1  width=100%>
                                                    <tr  align="center">
                                                    <td>Stok</td>
                                                    <td>Sisa</td>
                                                    <td>terjual</td>
                                                    </tr>
                                                </table></center>
                                                </th>
                                                <th>Terjual</th>
                                                <th>Sisa</th>
                                                <th>Harga Jual</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; foreach($databuku as $data):?>
                                            <?php if($data->kelas_buku > 9):$no++?>
                                                <tr  align="center">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_buku?></td>
                                                    <td><?php echo $data->kelas_buku?></td>
                                                    <td><center>
                                                    <?php foreach($laporansisa as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td><?php echo $key->stok_awal?></td>
                                                        <td><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td>120</td>
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td><center>
                                                    
                                                    <?php foreach($data2019 as $key):
                                                            if($data->id_buku == $key->id_buku):?>
                                                    <table width=100%>
                                                        <tr  align="center">
                                                        <td width=33%><?php echo $key->stok_awal?></td>
                                                        <td width=33%><?php echo $key->stok_awal-$key->stok_sisa?></td>
                                                        <td width=33%><?php echo $key->stok_sisa?></td>
                                                        
                                                        </tr>
                                                    </table></center>
                                                    <?php endif; endforeach?>
                                                    </td>
                                                    <td>Haha</td>
                                                    <td>haha</td>
                                                    <td><?php echo "Rp. ".$data->harga_buku?></td>
                                                </tr>
                                                <?php endif?>
                                            <?php endforeach?>
                                            <?php foreach($rekapan as $data):?>
                                            <!-- <tr>
                                                    <td></td><td></td><td>Total</td><td align="center"><center><?php echo $data->stok_awal." | ".($data->stok_awal-$data->stok_sisa)?></center></td> -->
                                            <?php endforeach?>
                                        </tbody>
                                    </table>
                    </div>
                <!-- untuk batas buku level SMA -->

                </div>
            </div>
        </div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->
</body>

</html>
