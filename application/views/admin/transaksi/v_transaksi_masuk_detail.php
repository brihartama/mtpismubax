<!DOCTYPE html>
<html lang="en">

<head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/images'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3" style="width: 70%; margin: 0 auto; padding: 20px;">


					<div class="center">

						<img src="<?php echo base_url('assets/images/user.png');?> "/> </div>



						<form action="<?php echo base_url().'admin/transaksi/simpan_transaksi_pesanan'?>" method="post" enctype="multipart/form-data" >
						<?php foreach($detailpembeli as $data):?>

						<div class="text-1">
							<h1><?php echo $data->nama_sekolah?> </h1>

						<div class="product_id" hidden><a href="product.html"><input type="text"
						name="xcustomer" value="<?php echo $data->id_sekolah?>"></a></div>

						<h5><i> ID Pelanggan : <?php echo $data->id_sekolah?>  </i> </h5>
						<h5> <?php echo $data->alamat_sekolah?> </h5>

						</div>


						<hr>
						
						<div class="text-1 phone">  Nara Hubung : <?php echo $data->no_hp?> </div> 
</div>
				<div class="card mb-3">
					
					<div class="card-body">

						<div class="table-responsive">

						<?php endforeach?>
							<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nomer</th>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Jumlah Barang</th>
										<th>Harga</th>
									</tr>
								</thead>

								<tbody>
									<?php $no=0; foreach($detailpesanan as $data): $no++?>
									<tr>
										<td><?php echo $no?></td>
										<td> <?php echo $data->nama_buku." Kelas ".$data->kelas_buku?><div class="product_id" hidden><a href="product.html"><input type="text" name="xbarang[]" value="<?php echo $data->id_buku?>"></a></div></td>
										<td><?php echo $data->harga_buku?></td>
										<td> <?php echo $data->jumlah_buku?><div class="product_id" hidden><a href="product.html"><input type="text" name="xbanyak[]" value="<?php echo $data->jumlah_buku?>"></a></div></td>
										<td> <?php echo $data->total_harga?><div class="product_id" hidden><a href="product.html"><input type="text" name="xtotal[]" value="<?php echo $data->total_harga?>"></a></div></td>
									</tr>
									<?php endforeach?>
									
                                    <tr>
									<?php foreach($totalharga as $data):?>
                                        <th colspan='3'>TOTAL HARGA</th><th colspan=2>Rp. <?php echo $data->total_harga?></th>
                                    <?php endforeach?>
									</tr>

								</tbody>
							</table>
								<input class="btn btn-info" type="submit" name="btn" value="KONFIRMASI PESANAN 1" /> 
							</form>
						</div>
					</div>
				</div>

			</div>


			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
