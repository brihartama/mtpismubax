
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_Buku extends CI_Model{

    private $_table = "tbl_buku";
    public $image = "default.jpg";

    public function getAll()
    {
        $query = $this->db->query("SELECT *FROM tbl_buku");
        return $query->result();
    }

    public function getAllsatu()
    {
        $query = $this->db->query("SELECT *FROM tbl_buku WHERE stok > 0");
        return $query->result();
    }

    public function simpan_buku($judul, $kelas, $harga, $stok, $photo)
    {
        $tanggal = date("Y-m-d H:i:s");
        $querysimpanbuku = $this->db->query("INSERT INTO tbl_buku(nama_buku, kelas_buku, harga_buku, stok, foto_barang) VALUES('$judul','$kelas','$harga', '$stok', '$photo')");
        return $querysimpanbuku;
    }
    public function simpan_buku_tanpa_image($judul, $kelas, $harga, $stok)
    {
        $tanggal = date("Y-m-d H:i:s");
        $querysimpanbuku = $this->db->query("INSERT INTO tbl_buku(nama_buku, kelas_buku, harga_buku, stok) VALUES('$judul','$kelas','$harga', '$stok')");
        return $querysimpanbuku;
    }

    public function edit_buku($where, $table){
        return $this->db->get_where($table, $where)->result();
    }

    public function update_buku($id, $nama, $kelas, $harga, $stok, $photo){
        $query = $this->db->query("UPDATE tbl_buku set nama_buku='$nama', kelas_buku='$kelas', harga_buku='$harga', stok = stok+'$stok', foto_barang='$photo' WHERE id_buku='$id'");
        return $query;
    }

    public function update_buku_tanpa_img($id, $nama, $kelas, $harga, $stok){
        $query = $this->db->query("UPDATE tbl_buku set nama_buku='$nama', kelas_buku='$kelas', harga_buku='$harga', stok=stok+'$stok' WHERE id_buku='$id'");
        return $query;
    }


    public function hapus($where, $table){
        $this->db->where($where);
		$this->db->delete($table);
    }

    public function delete_buku($id){
        $query = $this->db->query("DELETE FROM tbl_buku WHERE id_buku = '$id'");
        return $query;
    }

    public function simpan_buku_image($nama, $kelas, $harga, $stok, $photo){
        $tanggal = date("Y-m-d H:i:s");
        $gambar = $this->_uploadImage();
        $querysimpanbuku = $this->db->query("INSERT INTO tbl_buku(nama_buku, kelas_buku, harga_buku, stok, foto_barang) VALUES('$judul','$kelas','$harga', '$stok', '$gambar')");
        return $querysimpanbuku;
    }

    public function ambil_id(){
        $query = $this->db->query("SELECT id_buku from tbl_buku ORDER BY id_buku DESC LIMIT 1");
        return $query->row()->id_buku;
    }

    public function update_laporan_stok($id,$stok){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_stok_buku(id_buku,stok_awal,tanggal_update) VALUES('$id','$stok','$tanggal')");
        return $query;
    }

    // dibawah ini buat customer
    public function bukusd(){
        $query =$this->db->query("SELECT *FROM tbl_buku WHERE kelas_buku < 7 AND stok > 0");
        return $query->result();
    }

    public function simpan_stok($idbukunya, $stok){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_stok_buku(id_buku,stok_awal, tanggal_update) VALUES('$idbukunya','$stok','$tanggal')");
        return $query;
    }

    public function bukusmp(){
        $query =$this->db->query("SELECT *FROM tbl_buku WHERE kelas_buku < 10 AND kelas_buku > 6 AND stok > 0");
        return $query->result();
    }

    public function bukusma(){
        $query =$this->db->query("SELECT *FROM tbl_buku WHERE kelas_buku > 9 AND stok > 0");
        return $query->result();
    }

    public function cari_buku($cari){
        $query = $this->db->query("SELECT *FROM tbl_buku WHERE nama_buku LIKE '%$cari%' || kelas_buku LIKE '%$cari%'");
        return $query->result();
    }

    public function cek_stok($idbarang){
        $this->db->select('stok');
        $this->db->from('tbl_buku');
        $this->db->where('id_buku',$idbarang);
        return $this->db->get()->row()->stok;
    }

    public function detail_buku($id_buku){
        $query = $this->db->query("SELECT *FROM tbl_buku WHERE id_buku='$id_buku'");
        return $query->result();
    }

}