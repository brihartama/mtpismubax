
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_overview extends CI_Model{

    public function hitung_buku(){
        $query = $this->db->query("SELECT COUNT(*) as banyak_buku FROM tbl_buku");
        return $query->result();
    }

    public function hitung_pelanggan(){
        $query = $this->db->query("SELECT COUNT(*) as banyak_pelanggan FROM tbl_sekolah");
        return $query->result();
    }

    public function buku_terlaris(){
        $query = $this->db->query("SELECT sum(tbl_stok_buku.stok_sisa) as stok_sisa, tbl_buku.id_buku, tbl_buku.nama_buku, tbl_buku.kelas_buku, tbl_buku.harga_buku, tbl_buku.foto_barang
        FROM tbl_buku, tbl_stok_buku WHERE tbl_buku.id_buku = tbl_stok_buku.id_buku GROUP BY tbl_stok_buku.id_buku ORDER BY stok_sisa DESC limit 1");
        return $query->result();
    }

    public function sekolah_terbanyak(){
        $query = $this->db->query("SELECT sum(tbl_transaksi.banyak_barang) as banyak_barang, tbl_sekolah.id_sekolah, tbl_sekolah.nama_sekolah 
        FROM tbl_transaksi, tbl_sekolah WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah GROUP BY tbl_transaksi.id_sekolah ORDER BY banyak_barang DESC limit 1");
        return $query->result();

    }

}